package com.encrypt.data.controller;

import com.encrypt.data.service.AESService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class EncryptController {
    @Autowired
    AESService aesService;

    @GetMapping("/stringEncrypt")
    public ResponseEntity<?> stringEncrypt(
            @RequestParam(value = "string") String originalString
    ) {
        try {
            String secretKey = "Secret";
            String encryptedString = aesService.encrypt(originalString, secretKey);
            String decryptedString = aesService.decrypt(encryptedString, secretKey);
            HashMap<String, String> response = new HashMap<>();
            response.put("originalString", originalString);
            response.put("encryptedString", encryptedString);
            response.put("decryptedString", decryptedString);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
